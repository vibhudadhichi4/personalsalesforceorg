public class ApexAccountCreation {

    @AuraEnabled
    public static Account insertRecord(String accName){
        
        Account acc = new Account();
        acc.Name = accName;
        insert acc;
        
      return acc;  
    }
}