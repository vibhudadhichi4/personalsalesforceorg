public with sharing class Recent_Lead_Record {
    @AuraEnabled
    public static List<Lead> getLeads(Id recId){
		List<Lead> leadRecords = [SELECT id, name, status, email FROM Lead Where id=: recId Limit 50];
        return leadRecords;
            }
}