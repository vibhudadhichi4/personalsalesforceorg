public class ApexProductSearchController {
	 @AuraEnabled
    public static Product2 searchProduct(String searchProduct){
      Product2 lstSearchResult = [Select Name FROM Product2 WHERE Name = :searchProduct];
                
      return lstSearchResult;  
    }
}