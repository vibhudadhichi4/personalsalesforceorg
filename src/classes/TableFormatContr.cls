global with sharing class TableFormatContr {
    
    public TableFormatContr(){}
    
    @RemoteAction
    global static List<Account> getAccount () {
        
        List<Account> accList=[select Id,Name from Account];
        return accList;
       
    }
}