trigger UpdateAccount on Account (after insert,after update) {

    List<vibhu__AccountData__c> ListOfAccount = new List<vibhu__AccountData__c>();
    
    for(Account accList : trigger.new)
    {
        Account value = trigger.oldMap.get(accList.id);
        
        if(trigger.oldMap.get(accList.id).BillingStreet != accList.BillingStreet)
        {
            vibhu__AccountData__c accData = new vibhu__AccountData__c();
            accData.Name = accList.Name;
            ListOfAccount.add(accData);  
        }
    }
    
    try{
    
        insert ListOfAccount;
    }
    catch(System.DMLException ex)
    {
        System.debug('Exception is' +ex.getMessage());
    }
}