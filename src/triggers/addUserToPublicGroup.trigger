trigger addUserToPublicGroup on User (after insert) {
    List<GroupMember> memberList = new List<GroupMember>();
    for(User userId : Trigger.New) {
        if(userId.isActive) {
            GroupMember memberOfGroup = new GroupMember();
            String groupId = [Select Id,Name from Group where Name='Some Group'].Id;
            System.Debug('GroupId'+groupId);
            memberOfGroup.GroupId = groupId;
            System.Debug('userId'+userId.Id);
            memberOfGroup.UserOrGroupId = userId.Id;
            memberList.add(memberOfGroup);         
        }
    }
    if(!memberList.isEmpty()) {
        insert memberList;
    }
}